/**
  ******************************************************************************
  * @file     system_interface.h
  * @author  Nguyentaihau
  * @version 
  * @date    10-9-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  

#ifndef __UART1_BMS_
#define __UART1_BMS_
#include "uart1_system.h"


#define FRAME_BMS_LEN 17


typedef enum{
  BMS_IDLE  ,
  BMS_PROCESS 
}bms_state_t;


void Uart1_BMS_Init(void);
void Task_BMS(void);
void Uart1_BMS_sendRS485(uint8_t *iData, uint8_t NumByte);
bms_state_t Uart1_BMS_State(bms_state_t curr_state, bms_state_t *old_state);

#endif