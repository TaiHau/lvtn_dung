/**
  ******************************************************************************
  * @file     Uart2_internet.h
  * @author  Nguyentaihau
  * @version 
  * @date    10-9-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  

#ifndef __UART3_BLT_
#define __UART3_BLT_
/* Includes ------------------------------------------------------------------*/
#include "uart3_bluetooth.h"


#define FRAME_BLT_LEN 17

void uart3_blt_init(void);
void uart3_blt_send(uint8_t *pdata, uint8_t len);

#endif