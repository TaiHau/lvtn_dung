/**
  ******************************************************************************
  * @file     Uart3_bluetooth.c
  * @author  Nguyentaihau
  * @version 
  * @date    19-10-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  

#include "uart3_bluetooth.h"

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
 RxBuffUart_T  RxUart3;

/* Private function prototypes -----------------------------------------------*/

/*******************************************************************************
* Function name: UART3_Bluetooth_Init
* Description:
* Input: 
* Return:
*******************************************************************************/
void UART3_Bluetooth_Init(void){
  GPIO_InitTypeDef GPIO_InitStructure;

  
  /* Configure USART2 Rx as input floating */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
 
  
  /* Configure USART2 Tx as alternate function push-pull */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  
  
  USART_InitTypeDef USART_InitStructure;
  USART_InitStructure.USART_BaudRate = 9600;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  
  USART_DeInit(USART3);
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE);
  USART_Init(USART3,&USART_InitStructure);
  
  USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
  USART_Cmd(USART3,ENABLE);
}


void UART3_Bluetooth_Read_isr(void){
   //--------new
   Wr_buff(&RxUart3,RxBuffUART_size,(uint8_t)USART_ReceiveData(USART3));
}


void UART3_Bluetooth_write(uint8_t *iData,uint8_t NumByte){
  for(uint8_t i = 0;i < NumByte;i++){
    USART_SendData(USART3,*iData);
    while(USART_GetFlagStatus(USART3,USART_FLAG_TXE)==RESET);
    iData++;
  }
}
