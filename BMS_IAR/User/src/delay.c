/**
  ******************************************************************************
  * @file: delay.c
  * @brief:   
  * @author: GiaoThoaTech Team
  * @version: V0.1.0
  * @date: AUG-2017
	* @The copyrights belong to Giao Thoa Tech
  ******************************************************************************

  ******************************************************************************
  * @MCU peripheral usage:  
  *   $ None
  ******************************************************************************
**/ 


/* Includes ------------------------------------------------------------------*/
#include "delay.h"

/* Private define ------------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
/* Global variables ----------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/******************************************************************************
  *     FUNCTIONS 

  Strongly recommend: write code as following form

/
  * @brief  Initializes the GPIOx according to the specified parameters.
  * @param1  GPIOx : Select the GPIO peripheral number (x = A to I).
  * @param2 GPIO_Pin : This parameter contains the pin number
  *           This parameter can be one of the following values:
  *            @arg GPIO_Pin_0: Pin 0PIO_Mode_In_FL_No_IT: Input floating, no external interrupt
  * @retval 1: done
  *         0: fail
  *         None
  **/
//  void FunctionName(void)
//  {
//  }

  /******************************************************************************
**/ 


/**
  * @brief  Delay in microsecond
  * @param  time (us)
  * @retval       
  **/
void DelayUs(unsigned short time)
{
	while(time--)
	{     
		;
	}
}

/**
  * @brief  Delay in milisecond
  * @param  time (ms)
  * @retval       
  **/
void DelayMs(uint16_t time)  // fHSI = 16Mhz
{
  uint16_t i;
  for(i=0; i<time; i++) DelayUs(1000);		
}

/**
  * @brief  Wait 1 sec 
  * @param  None.
  * @retval None.
  * Note : TIM4 is configured for a system clock = 2MHz
  */
//void Delay1s(void)
//{
//  CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);
//
//  /* Configure TIM4 to generate an update event each 1 s */
//  TIM4_TimeBaseInit(TIM4_Prescaler_16384, 123);
//  /* Clear update flag */
//  TIM4_ClearFlag(TIM4_FLAG_Update);
//
//  /* Enable TIM4 */
//  TIM4_Cmd(ENABLE);
//
//  /* Wait 1 sec */
//  while ( TIM4_GetFlagStatus(TIM4_FLAG_Update) == RESET );
//
//  TIM4_ClearFlag(TIM4_FLAG_Update);
//
//  /* Disable TIM4 */
//  TIM4_Cmd(DISABLE);
//
//  CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, DISABLE);
//}