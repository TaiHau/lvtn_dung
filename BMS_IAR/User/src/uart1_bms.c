/**
  ******************************************************************************
  * @file     system_interface.h
  * @author  
  * @version 
  * @date    10-9-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  

// Giao tiep RS485

/* Includes ------------------------------------------------------------------*/
#include "uart1_bms.h"
#include "user_define.h"
#include "delay.h"
#include "uart2_esp.h"
#include "uart3_blt.h"


uint8_t Uart1_Bms_Shiff_frame[FRAME_BMS_LEN];
uint8_t Bms_frame_rec[FRAME_BMS_LEN];
uint8_t bms_flag_update = 0;
bms_state_t Bms_state = BMS_IDLE;
bms_state_t Bms_state_old = BMS_PROCESS;

static uint8_t Uart1_BMS_getFrame(uint8_t *pData, uint8_t CharterStart, uint8_t CharterStop, uint8_t len);

/*******************************************************************************
* Function name: Uart1_BMS_Init
* Description:
* Input: 
* Return:
*******************************************************************************/
void Uart1_BMS_Init(void){
  UART1_System_Init();
}

bms_state_t Uart1_BMS_State(bms_state_t curr_state, bms_state_t *old_state)
{
  bms_state_t state;
  *old_state = curr_state;
  switch(curr_state)
  {
  case BMS_IDLE:
    if (bms_flag_update == 1)
    {
      state = BMS_PROCESS;
    }
    break;
  case BMS_PROCESS:
    state = BMS_IDLE;
    break;
  default:
    break;
  }
  return state;
}

void Task_BMS(void)
{
  bms_flag_update = Uart1_BMS_getFrame(Bms_frame_rec, 'S', 'E', FRAME_BMS_LEN);
  Bms_state = Uart1_BMS_State(Bms_state, &Bms_state_old);
  if ((Bms_state == BMS_IDLE) && (Bms_state != Bms_state_old))
  {
    bms_flag_update = 0;
  }
  else if ((Bms_state == BMS_PROCESS) && (Bms_state != Bms_state_old))
  {
    Uart2_esp_send(Bms_frame_rec, FRAME_BMS_LEN);
    uart3_blt_send(Bms_frame_rec, FRAME_BMS_LEN);
  }
  else {;}
}
  
static uint8_t Uart1_BMS_getFrame(uint8_t *pData, uint8_t CharterStart, uint8_t CharterStop, uint8_t len){
  uint8_t Read_status;
  uint8_t Frame_status;
  uint8_t Data_val ;
  uint8_t i,k;

  Frame_status = 0;
  // lay data tu Buffer va kiem tra xem co Frame gui ve khong.
  while((RxUart1.Len > 0)&&(Frame_status == 0)){
      Read_status = Rd_buff(&RxUart1,RxBuffUART_size, &Data_val);
      // kiem tra data new
      if(Read_status == 1){
        // Shiff toan bo data sang phai 1 byte
        for( i = 0;i < len;i++){
           Uart1_Bms_Shiff_frame[len - i -1] = Uart1_Bms_Shiff_frame[len - i - 2];
        }
        // day data moi vao
        Uart1_Bms_Shiff_frame[0] = Data_val;
        // kiem tra Frame dung la 1 frame
            if((Uart1_Bms_Shiff_frame[len-1] == CharterStart) && (Uart1_Bms_Shiff_frame[0] == CharterStop)){
              // truyen frame len tren
              for( k = 0; k < len;k++){
                *pData = Uart1_Bms_Shiff_frame[len - k - 1];
                pData++;
                Uart1_Bms_Shiff_frame[len - k - 1] = 0;
              }
              Frame_status = 1;
            }
            else {Frame_status = 0;}
      }
  }
  // return 1 neu co Frame, 0 neu khong co Frame.
  return Frame_status;
}

void Uart1_BMS_sendRS485(uint8_t *iData, uint8_t NumByte){
  GPIO_SetBits(GPIOA,SEC_UART1_PIN);
  DelayMs(100);
  for(uint8_t i = 0;i < NumByte;i++){
    USART_SendData(USART1,*iData);
    while(USART_GetFlagStatus(USART1,USART_FLAG_TXE)==RESET);
    iData++;
  }
  GPIO_ResetBits(GPIOA,SEC_UART1_PIN);
}


