/**
  ******************************************************************************
  * @file     Uart1_system.h
  * @author  Nguyentaihau
  * @version 
  * @date    10-9-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  

#ifndef __UART1_SYSTEM_H
#define __UART1_SYSTEM_H
/* Includes ------------------------------------------------------------------*/

#include "stm32f10x_usart.h"
#include "stm32f10x_gpio.h"
#include "uart_rxbuff.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

#define SEC_UART1_PIN  GPIO_Pin_8

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

 extern RxBuffUart_T  RxUart1;
/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
void UART1_System_Init(void);
void UART1_System_Read_isr(void);



#endif