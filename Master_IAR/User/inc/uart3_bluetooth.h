/**
  ******************************************************************************
  * @file     Uart3_bluetooth.h
  * @author  Nguyentaihau
  * @version 
  * @date    19-10-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  

#ifndef __UART3_BLUETOOTH
#define __UART3_BLUETOOTH

#include "stm32f10x_usart.h"
#include "stm32f10x_gpio.h"
#include "uart_rxbuff.h"

extern RxBuffUart_T  RxUart3;

extern void UART3_Bluetooth_Init(void);
extern void UART3_Bluetooth_Read_isr(void);
extern void UART3_Bluetooth_write(uint8_t *iData, uint8_t NumByte);

#endif