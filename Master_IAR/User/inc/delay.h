/**
  ******************************************************************************
  * @file    delay.h 
  * @author  delay without using timer
  * @version V1.1.0
  * @date    Sep-2017
  * @brief   Header for delay.c module
  ******************************************************************************
  */ 
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _delay_H_
#define _delay_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
/* Private typedef ---------------------------------------------------------*/
/* User Define -------------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */
void DelayUs(unsigned short time);
void DelayMs(uint16_t time);

#endif
/*****************END OF delay.h**********************************************/